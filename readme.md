# Guide du prix libre  

## Principes Généraux

### Vocabulaire

à faire : décider d'un terme ou proposer différents termes pour désigner la pratique du prix libre :

    dénomination : prix, contribution, participation

    qualificatif : libre, conscient, responsable, nécessaire

    monnaie, dette, libre, locale etc.


### Dans quel contexte puis-je faire du prix libre ?

Le prix libre peut être utilisé pour financer de nombreuses activités et sur différentes temporalités :

    conférence (quelques heures en journée ou en soirée)

    événement, festival (quelques jours ou semaines)

    formation (1/2 jour à une semaine ou plus si temps fractionné)

    services (durée indéfinie)

    biens (livres, nourriture, loyer, bière...)


Le prix libre permet de rembourser ou de financer a posteriori un projet.
Le démarrage de l'activité suppose bien souvent la présence de fonds avant la réalisation de l'activité.

Si vous avez besoin d'argent pour débuter une activité, vous pouvez organiser une campagne de dons ou de prêt ou de souscription.

### Comment faire du prix libre ?

La communication est l'élément essentiel au bon fonctionnement du prix libre.

    Annoncer en amont que le prix libre est utilisé pour le financement de votre projet.

    Indiquer les coûts financiers engendrés par la réalisation de votre activité, en détaillant les postes.

    Renseigner l'usage de l'argent récolté. Est-ce que l'argent va servir à payer des personnes, à permettre d'organiser un prochain événement, à rembourser les coûts avancés, à maintenir les services, etc. et dans quelles proportions entre ces différents postes.


Ces informations doivent permettre aux participants ou usagers de décider en toute conscience du montant de leur contribution.

En plus des éléments précédents, vous pouvez rajouter d'autres informations pour aider les participants ou usagers :

    une référence avec le coût de prestations (café, bière, cinéma, SMIC horaire, etc.) pour placer le service/biens dans une échelle de prix connus.

    le temps bénévole pour l'organisation ou le bon fonctionnement de la prestation


### Quels sont les pièges à éviter lorsque l'on pratique le prix libre ?

Éviter de "prendre en otage" les participants restant à la fin d'une activité.
(par exemple : éviter d'annoncer 1h avant la fin que l'événement qu'il manque Xe pour rembourser les frais avancés pour l'événement et que comme il reste Y personnes, il "suffit" que chacun donne X/Ye)

Ne pas confondre Prix libre et Chapeau !

    Chapeau : dans le domaine du spectacle, il est courant de faire passer un chapeau à la fin de la représentation. L'intégralité du chapeau est remis à l'artiste/intervenant et ne donne pas lieu à déclaration (pas de cachet, pas de cotisations sociales ...)

    Prix libre : le montant de la recette collectée revient à l'organisateur qui décide de son affectation selon ses besoins (rémunération intervenant mais aussi : location de salle et de matériel, frais de communication, rémunération personnel technique, ...) Pour ne pas confondre avec le chapeau, il est préférable de demander le prix libre à l'entrée.


à faire : d'autres exemples ou conseils ?

### Pourquoi faire du prix libre ?

Le prix libre permet de financer solidairement et en conscience des activités, en fonction des moyens que chacun.e peut y mettre.

à faire : plus d'explication sur pourquoi c'est solidaire

## Cas pratiques et cas particuliers

###[exemple lieux à prix libre]

Ateliers de vélo
Pavillons Sauvages

### [exemple conférence à prix libre]

### [exemple service(s) à prix libre]

Réparation et/ou reconditionnement de pc
Connexion à Internet ou hébergement de machine à tetaneutral

### [exemple formation à prix libre]

Atelier d'auto-défense numérique au Laboratoire Ouvert Lyonnais

### [exemple événement à prix libre]
THSF (Toulouse Hacker Space Factory) à Toulouse : https://www.thsf.net/ --> PLN (participation libre et nécessaire) à l'entrée du public (c'est la même chose pour tous les évènements à Myrys)
Festival de conférences gesticulées de Lyon Mars 2017 : http://festiconfslyon.fr/prix-libre/   Participation libre et consciente (voir texte et visuel d'explication proposés à l'entrée)

### [exemple biens à prix libre]
Le "non marché" de Notre Dame des Landes, où on paye ses légumes à prix libre https://www.mediapart.fr/studio/panoramique/la-zad-ca-marche-ca-palabre-cest-pas-triste

### Facturation à prix libre

Pour facturer une prestation à prix libre, il suffit d'émettre une facture avec le montant décidé et versé par le participant ou l'usager de l'activité.

Exemple d'une conférence :
    - 200€ ont été récolté lors de la conférence à prix libre
    - la location de l'espace et les frais d'impression de communication s'élèvent à 90€
    Le bénéfice de l'événement est donc de 110€. Si cela a été décidé et renseigné en amont, le ou la conférencier·ère peut émettre une facture d'un montant de 110€.

Exemple d'un accompagnement ponctuel dans la définition d'un projet d'activité professionnelle :

    - la personne qui demande a dit le budget qu'elle estimait pouvoir mettre au regard de ce qu'elle estimait être le "prix du marché" pour ce type d'accompagnement

    - j'ai donné mes fourchettes et effectivement confirmé que le montant proposé rentrant dans cette fourchette était entendable pour moi 

    - nous avons convenu d'une rencontre de 2H, puis une demande complémentaire de préparation est arrivée avec proposition de la financer également

    - nous avons réalisé la réunion de travail de 2h (+ 1h de préparation) à laquelle j'ai proposé d'ajouter un feedback quelques jours après (+1h)

    - la personne m'a remis le montant convenu et j'ai fait une facture du montant reçu indiquant le mode et la date de règlement

    - nous avons convenu qu'une demande prochaine pourrait faire l'objet d'un autre montant en fonction des besoin et moyens du moment


### à faire : d'autres cas pratiques, car particuliers ou informations utiles pour l'usage du prix libre ?

## Ressources pour aller plus loin

### Sphère marchande : le troc, les crypto-monnaies, les monnaies libres, la dette etc.

    Vidéo sur l'argent dette : https://www.youtube.com/watch?v=ZE8xBzcLYRs

    Monnaie libre - site officiel : https://duniter.org/fr/comprendre/ /!\ à prendre avec des pincettes, le système duniter est critiqué /!\


###Sphère non-marchande
Conférence - une société non marchande : https://www.youtube.com/watch?v=aeH5YxC5b8E 

### Conférence, documentations

    Une fervente adepte du prix libre  Extrait d’une interview d’Elsa/Bavardages (2006) https://infokiosques.net/spip.php?article460

    Le prix libre : https://infokiosques.net/spip.php?article49

    Repensez l’argent et expérimentez le prix libre ! https://ploum.net/repensez-largent-et-experimentez-le-prix-libre/

    Le prix libre décolle-t-il ? : https://ploum.net/le-prix-libre-decolle-t-il/

    Le prix libre, une impossible utopie ? https://ploum.net/le-prix-libre-une-impossible-utopie/


### Exemples de présentation de la démarche

    "La participation consciente" comme modèle économique de l'Université du Nous http://universite-du-nous.org/a-propos-udn/son-modele-economique/

    "Prix libre et rétribution libre" (dans les Tiers Lieux, sur MoviLab) http://movilab.org/index.php?title=Prix_libre_et_r%C3%A9tribution_libre

    "Participation libre et consciente" pour un atelier d'autonomie électrique http://david.mercereau.info/wp-content/uploads/2017/06/Participation-libre-et-consciente-Autonomie-%C3%A9lectrique-20170923.pdf

    "Participation libre et consciente" comme invitation à observer et faire évoluer nos rapports à l'argent http://terre-deveil.blogspot.fr/p/participation-libre.html

    "libre contribution"  https://www.conscience33.fr/libre_participation.htm

    vidéo argumentaire sur le prix libre chez un professionnel https://www.youtube.com/watch?v=1eJ8HZy7pbo&feature=youtu.be

    adhésion à montant libre présenté comme telle :

    La monnaie autrement à Chambéry http://www.lamonnaieautrement.org/images/vieassociative/adhesionLMA_2016_VF_1page.pdf

    La Gonette, monnaie locale complémentaire de l'aire urbaine lyonnaise http://www.lagonette.org/adherez-re-adherez-directement-site/



### Méthode du "tas d'argent" des cercles restauratifs, …
à faire : compléter avec des liens



----------------------------------------------------------------- FIN DU DOCUMENT -----------------------------------------------------------------------------------------------------------------------------------------
# Organisation et timeline

Semaine 48 
(fin novembre) : Préparation d'une première version du document

Semaine 49 à 52/
1 (Décembre) : Diffusion du document dans nos collectifs et enrichissement du document sans diffuser l'URL du pad
--> on attend le samedi 8 Décembre pour diffuser, comme suggéré par FloLN ? OK pour moi (sqk)

Semaine 1 à 4
(Janvier ) : Synthèse et préparation d'une version 1.0 du document
à faire : organisation de réunion audio/vidéo si besoin

29 ou 30 Janvier : Restitution et diffusion publique du document


## à faire : 

    site dédié pour le document final ? (une bonne idée, il sera plus judicieux de le mettre en ligne via d'autre groupes plus actifs sur la documentation.)

    quelle licence utiliser

    --> CC-BY 4.0 ? CC-BY-SA 4.0 (permet d'en faire usage dans une activité "commerciale") (+1 pour le SA)

    Pourquoi pas CC0 ? +1 aussi ; ça veut dire qu'on n'a pas besoin d'être crédités, c'est ça ? En tout cas moi je n'en ressens pas le besoin (sqk) 

    CC0 - Du domaine public, ceci n'empèche pas de mettre les noms contribut-rice-eur-s ou de déclarer anonyme.

    utilisation d'un dépôt/wiki pour permettre la vie du document ?  (FAI maison peut fournir un dépôt gitlab ou une page de wiki,) (on doit aussi pouvoir utiliser le site MoviLab)

    Le wiki permettra la collaboration et un contenu à jour réalisé collectivement.



